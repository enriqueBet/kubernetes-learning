# Kubernetes Hands-On

### Installation

Installing `minikube`

```bash
$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
$ sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Starting

```bash
$ minikube start
```

Sync minikube

```bash
$ minikube -p minikube docker-env
$ eval $(minikube docker-env)
```

### 1. Creating a config map with some environmental variable

```bash
$ kubectl create configmap fresco-config \
 --from-literal=SERVER_URL=https://www.fresco.me
```

### 2. Create a pod from a configuration file

```bash
apiVersion: v1
kind: Pod
metadata:
	name: fresco-nginx-pod
spec:
	containers:
		- name: fresco-nginx-container
			image: nginx
			env:
				- name: SERVER_URL_ENV
					value: "https://www.fresco.me"
```

```bash
$ kubectl apply podconfig.yaml
```

```bash
$ kubectl run fresco-nginx-container --image=pod/fresco-config-pod
```

```bash
$ kubectl exec -it fresco-nginx-pod -- sh -c env | grep SERVER_URL_ENV
```

### 3. Create a Secret

```bash
$ kubectl create secret generic fresco-secret \
	--from-literal=user=admin \
	--from-literal=pass=pass
```

### Modify the config to add a secret and mount path

```bash
apiVersion: v1
kind: Pod
metadata:
	name: fresco-nginx-pod
spec:
	containers:
		- name: fresco-nginx-container
			image: nginx
			env:
				- name: SERVER_URL_ENV
					value: "https://www.fresco.me"
			volumeMounts:
				- name: test-volume
				  mountPath: "/etc/test"
	volumes:
		- name: test-volume
		  secret:
		  	secretName: fresco-secret
```

### Add a Persistent Volume

```bash
apiVersion: v1
kind: PersistentVolume
metadata:
	name: fresco-pv
spec:
	storageClassName: "manual"
	capacity:
		storage: 100M
	accessModes:
		- ReadWriteOnce
	hostPath:
		path: "/tmp/fresco"
```

